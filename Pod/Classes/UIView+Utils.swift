//
//  UIView+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Personal Finance on 07/07/2015.
//  Copyright (c) 2015 Personal Finance. All rights reserved.
//

import UIKit

@IBDesignable
public extension UIView {
    
    // MARK: - Utils atributes
    
    public var width: CGFloat {
        get {
            return self.frame.size.width
        }
        set {
            self.frame.size.width = newValue
        }
    }
    
    public var height: CGFloat {
        get {
            return self.frame.size.height
        }
        set {
            self.frame.size.height = newValue
        }
    }
    
    public var x: CGFloat {
        get {
            return self.frame.origin.x
        }
        set {
            self.frame.origin.x = newValue
        }
    }
    
    public var y: CGFloat {
        get {
            return self.frame.origin.y
        }
        set {
            self.frame.origin.y = newValue
        }
    }
    
    public var size: CGSize {
        get {
            return self.frame.size
        }
        set {
            self.frame.size = newValue
        }
    }
    
    // MARK: - Customizable item
    @IBInspectable public var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable public var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    public class func viewFromNib(aClass: NSObject.Type!) -> UIView? {
        
        let nameSpaceClassName = NSStringFromClass(aClass)
        let className = nameSpaceClassName.componentsSeparatedByString(".").last! as String
        
        let viewList = NSBundle.mainBundle().loadNibNamed(className, owner: nil, options: nil)
        
        for view in viewList {
            return view as? UIView
        }
        
        return nil
    }
    
    
    // MARK: - Localizable Utils
    
    public func getLocalizedText(key: String) -> String {
        return LocalizableHelper.getLocalizedText(key)
    }
    
    //MARK: view manipulation
    
    public func removeAllSubView() {
        while (self.subviews.count > 0) {
            self.subviews[0].removeFromSuperview()
        }
    }
}