//
//  UIImage+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Alexandre Mommers on 31/07/2015.
//  Copyright © 2015 Personal Finance. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {


    /**
     Resize image
     */
    func resizeImage(newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.drawInRect(CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    /**
     Resize image
     */
    func resizeImage(width width: CGFloat) -> UIImage {
        let percent = width * 100.0 / size.width
        let height = size.height * percent / 100.0
        let newSize = CGSizeMake(width, height)
        return resizeImage(newSize)
        
    }

    /**
     * take screenshot of view
     */
    public class func takeSnapshotOfView(view: UIView) -> UIImage {
        UIGraphicsBeginImageContext(view.bounds.size)

        view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: false)

        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }


    /**
     * draw image on image
     */
    public func drawImage(inputImage: UIImage, inRect frame: CGRect) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        self.drawInRect(CGRect(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height))
        inputImage.drawInRect(frame)
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

    /**
     * draw image on image
     */
    public func drawImage(inputImage: UIImage, atPosition position: CGPoint) -> UIImage {
        return drawImage(inputImage, inRect:CGRect(x: position.x, y: position.y, width: inputImage.size.width, height: inputImage.size.height))
    }

    public static func resizeImageWithKeepingProportion(imageToResize:UIImage, rect:CGRect) -> UIImage {

        //create image context
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        
        imageToResize.drawInRect(CGRect(
            x: rect.origin.x,
            y: rect.origin.y,
            width: imageToResize.size.width,
            height: imageToResize.size.height))
        
        
        //create image from context
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //return fresh image
        return newImage
    }
    
 
}