//
//  NSManagedObjectContext+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Alexandre Mommers on 17/07/2015.
//  Copyright © 2015 Personal Finance. All rights reserved.
//

import Foundation
import CoreData

public extension NSManagedObjectContext {
    
    public func getRandomEntity(className: String) throws -> AnyObject {
        
        let fetchRequest = NSFetchRequest()
        
        let entity = NSEntityDescription.entityForName(className, inManagedObjectContext: self)
        
        fetchRequest.entity = entity
        
        var error: NSError? = nil
        
        let myEntityCount = self.countForFetchRequest(fetchRequest, error: &error)
        
        fetchRequest.fetchOffset = myEntityCount - Int(arc4random() % UInt32(myEntityCount))
        fetchRequest.fetchLimit = 1
        
        let entities = try self.executeFetchRequest(fetchRequest)
        
        return entities[0]
    }
}