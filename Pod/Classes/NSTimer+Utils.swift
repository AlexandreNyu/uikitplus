//
//  NSTimer+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Alexandre Mommers on 10/07/2015.
//  Copyright © 2015 Personal Finance. All rights reserved.
//

import UIKit

public extension NSTimer {
    
    public class func cheduledTimerWithTimeInterval(ti: NSTimeInterval, block blockToExecute: () -> Void, userInfo: AnyObject?, repeats yesOrNo: Bool) {
        
        let timerToRunDelegate = NSTimer.scheduledTimerWithTimeInterval(ti,
            target: NSBlockOperation(block: blockToExecute),
            selector:#selector(NSOperation.main),
            userInfo: userInfo,
            repeats: yesOrNo)
        
        NSRunLoop.currentRunLoop().addTimer(timerToRunDelegate, forMode: NSDefaultRunLoopMode)
        
    }
}
