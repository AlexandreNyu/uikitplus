//
//  UIViewController+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Personal Finance on 06/07/2015.
//  Copyright (c) 2015 Personal Finance. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    public func addAndShowChildViewController(controller:UIViewController) {
        self.addAndShowChildViewController(controller, withSpecificView: self.view)
    }
    
    public func addAndShowChildViewController(controller:UIViewController, withSpecificView:UIView) {
        self.addAndShowChildViewController(controller, withSpecificView: withSpecificView, shouldAdaptView:true)
    }
    
    public func addAndShowChildViewController(controller:UIViewController, withSpecificView:UIView, shouldAdaptView:Bool) {
        if (shouldAdaptView) {
            controller.view.frame.size = withSpecificView.bounds.size
        }
        self.addChildViewController(controller)
        withSpecificView.addSubview(controller.view)
        controller.didMoveToParentViewController(self)
        
        
    }
}