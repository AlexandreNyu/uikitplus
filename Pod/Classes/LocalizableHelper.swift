//
//  LocalizableHelper.swift
//  Mon.iPhone.PF
//
//  Created by Alexandre Mommers on 20/07/2015.
//  Copyright © 2015 Personal Finance. All rights reserved.
//

import UIKit

public class LocalizableHelper: NSObject {
    
    public class func getLocalizedText(key: String?) -> String {
        if (key == nil) {
            return ""
        }
        
        #if TARGET_INTERFACE_BUILDER
            var bundle = NSBundle(forClass: self.dynamicType)
            return bundle.localizedStringForKey(key, value:"", table: nil)
        #else
            return NSLocalizedString(key!, comment:"");
        #endif
    }
    
}
