//
//  UIFont+Utils.swift
//  Mon.iPhone.PF
//
//  Created by Alexandre Mommers on 23/07/2015.
//  Copyright © 2015 Personal Finance. All rights reserved.
//

import Foundation
import UIKit

public extension UIFont {
    
    public class func displayFontsOnLog() {
        for familyName in UIFont.familyNames() {
            NSLog("family name %@", familyName)
            
            for fontName in UIFont.fontNamesForFamilyName(familyName) {
                NSLog("font name %@", fontName)
            }
            
        }
    }
}