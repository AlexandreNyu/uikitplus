# iOSUtils

[![CI Status](http://img.shields.io/travis/Alexandre Mommers/iOSUtils.svg?style=flat)](https://travis-ci.org/Alexandre Mommers/iOSUtils)
[![Version](https://img.shields.io/cocoapods/v/iOSUtils.svg?style=flat)](http://cocoapods.org/pods/iOSUtils)
[![License](https://img.shields.io/cocoapods/l/iOSUtils.svg?style=flat)](http://cocoapods.org/pods/iOSUtils)
[![Platform](https://img.shields.io/cocoapods/p/iOSUtils.svg?style=flat)](http://cocoapods.org/pods/iOSUtils)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

iOSUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "iOSUtils"
```

## Author

Alexandre Mommers, alexandre.mommers@gmail.com

## License

iOSUtils is available under the MIT license. See the LICENSE file for more info.
